# PHP-NGINX-MARIADB Minimal Docker

A simple docker configuration for local development that includes:

-   PHP - server side language
-   Nginx - web server
-   MariadDB - database
